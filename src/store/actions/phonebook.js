import * as actionTypes from './actionTypes';
import axios from 'axios';

export const waitPhonebookData = () => {
    return {
        type: actionTypes.WAIT_PHONEBOOK
    }
}

export const setPhonebookData = (phonebookData) => {
    return {
        type: actionTypes.SET_PHONEBOOK,
        phonebookData: phonebookData
    }
}

export const getPhonebookData = (value) => {
    return dispatch => {
        dispatch(waitPhonebookData())
        axios.get( `https://journal.bsuir.by/api/v1/portal/phoneBook?searchValue=${value}` )
            .then( response => {
               dispatch( setPhonebookData(response.data))
            } )
            .catch( error => {
               
            } );
    }
}