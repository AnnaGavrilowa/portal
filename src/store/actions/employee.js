import * as actionTypes from './actionTypes';
import axios from 'axios';

export const waitEmployeeDetails = () => {
    return {
        type: actionTypes.WAIT_EMPLOYEE_DETAILS
    }
}

export const setEmployeeDetails = (employeeDetails) => {
    return {
        type: actionTypes.SET_EMPLOYEE_DETAILS,
        employeeDetails: employeeDetails
    }
}

export const getEmployeeDetails = (employeeID) => {
    return dispatch => {
        dispatch(waitEmployeeDetails())
        axios.get( `https://journal.bsuir.by/api/v1/portal/employees/details?employeeId=${employeeID}` )
            .then( response => {
               dispatch( setEmployeeDetails(response.data))
            } )
            .catch( error => {
               
            } );
    }
}