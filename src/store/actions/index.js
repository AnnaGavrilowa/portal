export {
    getWeekNumber,
    getScheduleDownload,
    getEmployees,
    getEmployeeSchedule,
    getEmployeeAnnouncements,
    setCheckedWeeks,
    setCheckedSubgroups
} from './schedule';

export {
    getDepartments,
    getDepartmentStaff,
    getDepartmentAnnouncements
} from './departments'

export {
    getPhonebookData
} from './phonebook'

export {
    getEmployeeDetails
} from './employee'