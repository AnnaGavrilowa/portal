import * as actionTypes from './actionTypes';
import axios from 'axios';

export const setWeekNumber = (weekNumber) => {
    return {
        type: actionTypes.SET_WEEKNUMBER,
        weekNumber: weekNumber
    }
}

export const getWeekNumber = () => {
    return dispatch => {
        axios.get( 'https://journal.bsuir.by/api/v1/week' )
            .then( response => {
               dispatch( setWeekNumber(response.data))
            } )
            .catch( error => {
               
            } );
    }
}

export const waitScheduleDownload = (targetDownload) => {
    return {
        type: actionTypes.WAIT_DEPARTMENT_SCHEDULE_DOWNLOAD,
        targetDownload: targetDownload
    }
}

export const setScheduleDownload = (targetDownload) => {
    return {
        type: actionTypes.SET_DEPARTMENT_SCHEDULE_DOWNLOAD,
        targetDownload: targetDownload
    }
}

export const getScheduleDownload = (departmentId, url, fileName, targetDownload) => {
    return dispatch => {
        if (fileName.substring(19,23) === 'каф.') fileName = fileName.substring(0,19) + fileName.substring(23)
        dispatch(waitScheduleDownload(targetDownload))
        axios({
            url: `${url}${departmentId}`,
            method: 'GET',
            responseType: 'blob',
            headers: {
                'Accept': 'application/vnd.ms-excel',
                'Content-Type': 'application/json;charset=UTF-8'
            }
          }).then((response) => {
                const url = window.URL.createObjectURL(new Blob([response.data], {type: 'application/vnd.ms-excel'}));
                const link = document.createElement('a');
                link.href = url;
                link.setAttribute('download', `${fileName}.xlsx`);
                document.body.appendChild(link);
                link.click();
                document.body.removeChild(link); 
                window.URL.revokeObjectURL(url)

                dispatch(setScheduleDownload(targetDownload))
          });
    }
}

export const waitEmployees = () => {
    return {
        type: actionTypes.WAIT_EMPLOYEES
    }
}

export const setEmployees = (employeesData) => {
    return {
        type: actionTypes.SET_EMPLOYEES,
        employeesData: employeesData
    }
}

export const getEmployees = (searchText) => {
    return dispatch => {
        dispatch(waitEmployees())
        axios.get( `https://journal.bsuir.by/api/v1/portal/employeeSchedule/employee?employeeFio= ${searchText}` )
            .then( response => {
               dispatch( setEmployees(response.data))
            } )
            .catch( error => {
               
            } );
    }
}

export const waitEmployeeSchedule = () => {
    return {
        type: actionTypes.WAIT_EMPLOYEE_SCHEDULE
    }
}

export const setEmployeeSchedule = (employeeScheduleData) => {
    return {
        type: actionTypes.SET_EMPLOYEE_SCHEDULE,
        employeeScheduleData: employeeScheduleData
    }
}

export const getEmployeeSchedule = (employeeID) => {
    return dispatch => {
        dispatch(waitEmployeeSchedule())
        axios.get( `https://journal.bsuir.by/api/v1/portal/employeeSchedule?employeeId=${employeeID}` )
            .then( response => {
               dispatch( setEmployeeSchedule(response.data))
               dispatch (getEmployeeAnnouncements(employeeID))
            } )
            .catch( error => {
               
            } );
    }
}

export const waitEmployeeAnnouncements = () => {
    return {
        type: actionTypes.WAIT_EMPLOYEE_ANNOUNCEMENTS
    }
}

export const setEmployeeAnnouncements = (employeeId, employeeAnnouncementsData) => {
    return {
        type: actionTypes.SET_EMPLOYEE_ANNOUNCEMENTS,
        employeeId: employeeId,
        employeeAnnouncementsData: employeeAnnouncementsData
    }
}

export const getEmployeeAnnouncements = (employeeId) => {
    return dispatch => {
        dispatch(waitEmployeeAnnouncements())
        axios.get( `https://journal.bsuir.by/api/v1/announcementEmployee?employeeId=${employeeId}` )
            .then( response => {
               dispatch( setEmployeeAnnouncements(employeeId, response.data))
            } )
            .catch( error => {
               
            } );
    }
}

export const setCheckedWeeks = (value) => {
    return {
        type: actionTypes.SET_CHECKED_WEEKS,
        value: value
    }
}

export const setCheckedSubgroups = (value) => {
    return {
        type: actionTypes.SET_CHECKED_SUBGROUPS,
        value: value
    }
}