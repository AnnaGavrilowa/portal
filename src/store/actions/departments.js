import * as actionTypes from './actionTypes';
import axios from 'axios';

export const setDepartments = (departmentsData) => {
    return {
        type: actionTypes.SET_DEPARTMENTS,
        departmentsData: departmentsData
    }
}

export const getDepartments = () => {
    return dispatch => {
        axios.get( 'https://journal.bsuir.by/api/v1/department' )
            .then( response => {
               dispatch( setDepartments(response.data))
            } )
            .catch( error => {
               
            } );
    }
}

export const waitDepartmentStaff = () => {
    return {
        type: actionTypes.WAIT_DEPARTMENTSTAFF
    }
}

export const setDepartmentStaff = (departmentId, departmentStaffData) => {
    return {
        type: actionTypes.SET_DEPARTMENTSTAFF,
        departmentId: departmentId,
        departmentStaffData: departmentStaffData
    }
}

export const getDepartmentStaff = (departmentId) => {
    return dispatch => {
        dispatch(waitDepartmentStaff())
        axios.get( `https://journal.bsuir.by/api/v1/portal/employees?departmentId=${departmentId}` )
            .then( response => {
               dispatch( setDepartmentStaff(departmentId, response.data))
            } )
            .catch( error => {
               
            } );
    }
}

export const waitDepartmentAnnouncements = () => {
    return {
        type: actionTypes.WAIT_DEPARTMENTANNOUNCEMENTS
    }
}

export const setDepartmentAnnouncements = (departmentId, departmentAnnouncementsData) => {
    return {
        type: actionTypes.SET_DEPARTMENTANNOUNCEMENTS,
        departmentId: departmentId,
        departmentAnnouncementsData: departmentAnnouncementsData
    }
}

export const getDepartmentAnnouncements = (departmentId) => {
    return dispatch => {
        dispatch(waitDepartmentAnnouncements())
        axios.get( `https://journal.bsuir.by/api/v1/announcementDepartment?departmentId=${departmentId}` )
            .then( response => {
               dispatch( setDepartmentAnnouncements(departmentId, response.data))
            } )
            .catch( error => {
               
            } );
    }
}