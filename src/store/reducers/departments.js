import * as actionTypes from '../actions/actionTypes';

const initialState = {
    departments: [],
    //departmentStaff: [],
    departmentStaff: null,
    departmentAnnouncements: null,
    loading: false,
};

const setDepartments = ( state, action ) => {
    return {
        ...state,
        departments: state.departments.concat(action.departmentsData)
    }
};

const waitDepartmentStaff = (state, action) => {
    return {
        ...state,
        loading: true
    }
}

const setDepartmentStaff = ( state, action ) => {
    return {
        ...state,
        departmentStaff: {departmentId: action.departmentId, departmentStaffData: action.departmentStaffData},
        //departmentStaff: [...state.departmentStaff, {departmentId: action.departmentId, departmentStaffData: action.departmentStaffData}],
        loading: false
    }
};

const waitDepartmentAnnouncements = (state, action) => {
    return {
        ...state,
        loading: true
    }
}

const setDepartmentAnnouncements = ( state, action ) => {
    return {
        ...state,
        departmentAnnouncements: {departmentId: action.departmentId, departmentAnnouncementsData: action.departmentAnnouncementsData},
        loading: false
    }
};

const reducer = ( state = initialState, action ) => {
    switch ( action.type ) {
        case actionTypes.SET_DEPARTMENTS: return setDepartments( state, action );
        case actionTypes.WAIT_DEPARTMENTSTAFF: return waitDepartmentStaff( state, action );
        case actionTypes.SET_DEPARTMENTSTAFF: return setDepartmentStaff( state, action );
        case actionTypes.WAIT_DEPARTMENTANNOUNCEMENTS: return waitDepartmentAnnouncements( state, action );
        case actionTypes.SET_DEPARTMENTANNOUNCEMENTS: return setDepartmentAnnouncements( state, action );
        default: return state;
    }
};

export default reducer;