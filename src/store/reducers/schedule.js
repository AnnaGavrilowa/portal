import * as actionTypes from '../actions/actionTypes';

const initialState = {
    weekNumber: '',
    loading: false,
    loadingEmployees: false,
    employees: [],
    employeeSchedule: null,
    employeeAnnouncements: null,
    checkedWeeks: [true,true,true,true],
    checkedSubgroups: [true, true]
};

const setWeekNumber = ( state, action ) => {
    return {
        ...state,
        weekNumber: action.weekNumber
    }
};

const waitScheduleDownload = (state, action) => {
    if (action.targetDownload === 'd') {
        return {
            ...state,
            loading: true
        }
    }   
    if (action.targetDownload === 'e') {
        return {
            ...state
        }
    }  
}

const setScheduleDownload = ( state, action ) => {
    if (action.targetDownload === 'd') {
        return {
            ...state,
            loading: false
        }
    }   
    if (action.targetDownload === 'e') {
        return {
            ...state
        }
    }  
};

const waitEmployees = (state, action) => {
    return {
        ...state,
        loadingEmployees: true,
        employees: []
    }
}

const setEmployees = ( state, action ) => {
    return {
        ...state,
        employees: state.employees.concat(action.employeesData),
        loadingEmployees: false
    }
};

const waitEmployeeSchedule = (state, action) => {
    return {
        ...state,
        loading: true,
    }
}

const setEmployeeSchedule = ( state, action ) => {
    return {
        ...state,
        employeeSchedule: action.employeeScheduleData,
        loading: false
    }
};

const waitEmployeeAnnouncements = (state, action) => {
    return {
        ...state,
        loading: true
    }
}

const setEmployeeAnnouncements = ( state, action ) => {
    return {
        ...state,
        employeeAnnouncements: {employeeId: action.employeeId, employeeAnnouncementsData: action.employeeAnnouncementsData},
        loading: false
    }
};

const setCheckedWeeks = ( state, action ) => {
    let newValue = [...action.value]
    return {
        ...state,
        checkedWeeks: newValue
    }
}

const setCheckedSubgroups = ( state, action ) => {
    let newValue = [...action.value]
    return {
        ...state,
        checkedSubgroups: newValue
    }
}

const reducer = ( state = initialState, action ) => {
    switch ( action.type ) {
        case actionTypes.SET_WEEKNUMBER: return setWeekNumber( state, action );
        case actionTypes.WAIT_DEPARTMENT_SCHEDULE_DOWNLOAD: return waitScheduleDownload( state, action );
        case actionTypes.SET_DEPARTMENT_SCHEDULE_DOWNLOAD: return setScheduleDownload( state, action );
        case actionTypes.WAIT_EMPLOYEES: return waitEmployees( state, action );
        case actionTypes.SET_EMPLOYEES: return setEmployees( state, action );
        case actionTypes.WAIT_EMPLOYEE_SCHEDULE: return waitEmployeeSchedule( state, action );
        case actionTypes.SET_EMPLOYEE_SCHEDULE: return setEmployeeSchedule( state, action );
        case actionTypes.WAIT_EMPLOYEE_ANNOUNCEMENTS: return waitEmployeeAnnouncements( state, action );
        case actionTypes.SET_EMPLOYEE_ANNOUNCEMENTS: return setEmployeeAnnouncements( state, action );
        case actionTypes.SET_CHECKED_WEEKS: return setCheckedWeeks( state, action );
        case actionTypes.SET_CHECKED_SUBGROUPS: return setCheckedSubgroups( state, action );
        default: return state;
    }
};

export default reducer;