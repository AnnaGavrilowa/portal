import * as actionTypes from '../actions/actionTypes';

const initialState = {
    employeeDetails: null,
    loading: false
};

const waitEmployeeDetails = (state, action) => {
    return {
        ...state,
        loading: true
    }
}

const setEmployeeDetails = ( state, action ) => {
    let details = {...action.employeeDetails}
    return {
        ...state,
        employeeDetails: details,
        loading: false
    }
};

const reducer = ( state = initialState, action ) => {
    switch ( action.type ) {
        case actionTypes.WAIT_EMPLOYEE_DETAILS: return waitEmployeeDetails( state, action );
        case actionTypes.SET_EMPLOYEE_DETAILS: return setEmployeeDetails( state, action );
        default: return state;
    }
};

export default reducer;