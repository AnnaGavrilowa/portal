import * as actionTypes from '../actions/actionTypes';

const initialState = {
    phonebookData: [],
    loading: false
};

const waitPhonebookData = (state, action) => {
    return {
        ...state,
        loading: true
    }
}

const setPhonebookData = ( state, action ) => {
    return {
        ...state,
        phonebookData: [...action.phonebookData],
        loading: false
    }
};

const reducer = ( state = initialState, action ) => {
    switch ( action.type ) {
        case actionTypes.WAIT_PHONEBOOK: return waitPhonebookData( state, action );
        case actionTypes.SET_PHONEBOOK: return setPhonebookData( state, action );
        default: return state;
    }
};

export default reducer;