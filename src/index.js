import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import reportWebVitals from './reportWebVitals';
import { BrowserRouter } from 'react-router-dom';
import { Provider } from 'react-redux'
import { createStore, applyMiddleware, compose, combineReducers } from 'redux';
import thunk from 'redux-thunk';

import 'bootstrap/dist/css/bootstrap.min.css';
import 'react-bootstrap-typeahead/css/Typeahead.css';
import "bootstrap-tooltip-custom-class/bootstrap-v4/tooltip/dist/css/bootstrap-tooltip-custom-class.css";

import scheduleReducer from './store/reducers/schedule'
import departmentsReducer from './store/reducers/departments'
import phonebookReducer from './store/reducers/phonebook'
import employeeReducer from './store/reducers/employee'

const rootReducer = combineReducers({
    schedule: scheduleReducer,
    departments: departmentsReducer,
    phonebook: phonebookReducer,
    employee: employeeReducer
});

// const store = createStore(rootReducer(//, composeEnhancers(
//   applyMiddleware(thunk)
// )
// );

//const store = createStore(rootReducer);

const composeEnhancers = process.env.NODE_ENV === 'development' ? window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ : null || compose;

//const store = createStore(rootReducer, applyMiddleware(thunk))

const store = createStore(rootReducer, composeEnhancers(
  applyMiddleware(thunk)
));

ReactDOM.render(
  <Provider store={store}>
    <BrowserRouter>
          <App />
    </BrowserRouter>
  </Provider>,
  document.getElementById('root')
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
