import {parseDate} from './generalService'

export const getTerm = () => {
    const today = new Date();
    const mm = today.getMonth() + 1; 
    const dd = today.getDate();
    if (mm > 6 || (mm === 1 && dd < 6)){
      return 'осенний';
    } else {
      return 'весенний';
    }
}

export const getCurrentEducationalYear = () => {
    const today = new Date();
    const mm = today.getMonth() + 1; 
    let yyyy = today.getFullYear();

    if (mm < 7) {
      yyyy -= 1;
    }
    return yyyy;
}

export const getWeekDay = (dateString) => {  //format XX.XX.XXXX
    const date = new Date(dateString.split('.', 3).reverse().join('-'));

    switch (date.getDay()) {
      case 1:
        return 'Понедельник';
      case 2:
        return 'Вторник';
      case 3:
        return 'Среда';
      case 4:
        return 'Четверг';
      case 5:
        return 'Пятница';
      case 6:
        return 'Суббота';
      case 0:
        return 'Воскресенье';
      default:
          return null
    }
}

export const calculateEndOfLesson = (startTime) => {
    let date = new Date()
    date.setHours(+startTime.substring(0,2))
    date.setMinutes(+startTime.slice(-2))
    date.setHours(date.getHours() + 1);
    date.setMinutes(date.getMinutes() + 20);
    return date.getHours().toString() + ':' + date.getMinutes().toString()
}

export const parseCurrentSchedule = (scheduleData) => {
    const today = new Date();
    const tomorrow = new Date();
    tomorrow.setDate(today.getDate() + 1)

    let [todaySchedule, tomorrowSchedule] = [...scheduleData]
    todaySchedule = {...todaySchedule, dayNumber: 'Сегодня ' + parseDate(today) + ' - ' + getWeekDay(parseDate(today))}
    tomorrowSchedule = {...tomorrowSchedule, dayNumber: 'Завтра ' + parseDate(tomorrow) + ' - ' + getWeekDay(parseDate(tomorrow))}
    
    let todayScheduleList = [], tomorowScheduleList = []
    todayScheduleList.push(todaySchedule)
    tomorowScheduleList.push(tomorrowSchedule)

    return [todayScheduleList, tomorowScheduleList]
}

export const setGroups = (groupData) => {
  const sortedGroups = groupData.sort()
  const reducedGroupKeys = sortedGroups.map(e => { return e.slice(0, e.indexOf('#')-1) })

  Array.prototype.contains = function(v) {
    for (var i = 0; i < this.length; i++) {
      if (this[i] === v) return true;
    }
    return false;
  };

  Array.prototype.unique = function() {
    var arr = [];
    for (var i = 0; i < this.length; i++) {
      if (!arr.contains(this[i])) {
        arr.push(this[i]);
      }
    }
    return arr;
  }

  const uniqueGroupKeys = reducedGroupKeys.unique()

  const countOccurrences = (arr, val) => arr.reduce((a, v) => (v === val ? a + 1 : a), 0);

  let amountOfUniqueEntries = []
  for (let i = 0; i < uniqueGroupKeys.length; i++) 
    amountOfUniqueEntries.push(countOccurrences(reducedGroupKeys, uniqueGroupKeys[i]))

  const resultGroups = []
  for (let i = 0; i < amountOfUniqueEntries.length; i++){
    if (amountOfUniqueEntries[i] > 1) {
    
      let amount = 0
    
      for (let j = 0; j < amountOfUniqueEntries[i]; j++) {
        let str = (groupData[i+j].match("#(.*)#"))
        amount+= Number.parseInt((str[1]),10)}
    
      resultGroups.push({
        groupNumber: sortedGroups[i].slice(0, sortedGroups[i].indexOf('#')) + '-' + (amountOfUniqueEntries[i]+1),
        studentAmount: amount,
        code: sortedGroups[i].substring(sortedGroups[i].lastIndexOf('#')+1)
      })}
    else {
      let str = (groupData[i].match("#(.*)#"))
      resultGroups.push({
        groupNumber: sortedGroups[i].slice(0, sortedGroups[i].indexOf('#')),
        studentAmount: Number.parseInt((str[1]),10),
        code: sortedGroups[i].substring(sortedGroups[i].lastIndexOf('#')+1)
      })}
  }
  return resultGroups
}

export const filterScheduleByWeek = (scheduleData, filters) => {
  let filteredScheduleData = []
  let result = [...scheduleData]
  for (let i = 0; i < result.length; i++) {
    result[i] = {...scheduleData[i]}
  }

  for (let i = 0; i < scheduleData.length; i++) {
    filteredScheduleData = []
    for (let j = 0; j < scheduleData[i].schedule.length; j++) {
      for (let k = 0; k < scheduleData[i].schedule[j].weekNumber.length; k++) {
        if (filters[scheduleData[i].schedule[j].weekNumber[k]-1] === true) {
          filteredScheduleData.push(scheduleData[i].schedule[j])
          break
        }
      }
    }
    result[i].schedule = [...filteredScheduleData]
  }
  return result
}

export const filterScheduleBySubgroup = (scheduleData, filters) => {
  let filteredScheduleData = []
  let result = [...scheduleData]
  for (let i = 0; i < result.length; i++) {
    result[i] = {...scheduleData[i]}
  }

  for (let i = 0; i < scheduleData.length; i++) {
    filteredScheduleData = []
    for (let j = 0; j < scheduleData[i].schedule.length; j++) {
      if (filters[0] === true && filters[1] === true) {
          filteredScheduleData.push(scheduleData[i].schedule[j])
      }
      if (filters[0] === true && filters[1] === false) {
        if (scheduleData[i].schedule[j].numSubgroup === 1 || scheduleData[i].schedule[j].numSubgroup === 0) {
          filteredScheduleData.push(scheduleData[i].schedule[j])
        }
        
      }
      if (filters[0] === false && filters[1] === true) {
        if (scheduleData[i].schedule[j].numSubgroup === 2 || scheduleData[i].schedule[j].numSubgroup === 0) {
          filteredScheduleData.push(scheduleData[i].schedule[j])
        }
        
      }
      if (filters[0] === false && filters[1] === false) {
        if (scheduleData[i].schedule[j].numSubgroup === 0) {
          filteredScheduleData.push(scheduleData[i].schedule[j])
        }
        
      }
    }
    result[i].schedule = [...filteredScheduleData]
  }
  return result
}

export const isScheduleAvailable = () => {
  const today = new Date();
  return today.getMonth() === 6 && today.getDate() >= 5 || today.getMonth() === 7 && today.getDate() <= 15
}