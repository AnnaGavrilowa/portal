export const parseDate = (date) => {
    let d = date.getDate();
    let m = date.getMonth()+1;
    let y = date.getFullYear();

    if (d < 10) d = '0' + d
    if (m < 10) m = '0' + m

    return d + '.' + m + '.' + y
}

export const phoneNumberModification = (phoneNumber) => {
    let modifiedPhoneNumber = ''
    if (phoneNumber) {
        modifiedPhoneNumber = phoneNumber.substring(0, 4) + ' (' + 
        phoneNumber.substring(4,6) + ') ' +
        phoneNumber.substring(6,9) + '-' +
        phoneNumber.substring(9,11) + '-' +
        phoneNumber.substring(11,phoneNumber.length);
    }

    return modifiedPhoneNumber
}

export const transform = (value) => {
    let newUrl = '';

    if (value.search('.com')) {
      for (let i = 0; i < value.search('.com') + 4; i++) {
        newUrl += value.charAt(i);
      }
    } else if (value.search('.ru')) {
      for (let i = 0; i < value.search('.ru') + 3; i++) {
        newUrl += value.charAt(i);
      }
    } else if (value.search('.by')) {
      for (let i = 0; i < value.search('.by') + 3; i++) {
        newUrl += value.charAt(i);
      }
    }
    return newUrl;
  }