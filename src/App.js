import { Route, Switch } from 'react-router-dom';

import Layout from './hoc/Layout';
import HomePage from './components/HomePage/HomePage'
import About from './components/About/About'
import departmentStaffList from './components/DepartmentStaff/DepartmentSaffList'
import departmentAnnouncements from './components/DepartmentAnnouncements/DepartmentAnnouncements'
import departmentSchedule from './components/DepartmentSchedule/DepartmentSchedule'
import employeeSchedule from './components/EmployeeSchedule/EmployeeSchedule'
import phonebook from './components/PhoneBook/Phonebook'
import employeeDetails from './components/EmployeeDetails/EmployeeDetails'
import notFound from './components/UI/NotFound/NotFound'

function App() {
  return (
    <div className="App">
        <Layout>
          <Switch>
            <Route path="/about" exact component={About} />
            <Route path="/announcements" exact component={departmentAnnouncements} />
            <Route path="/departments" exact component={departmentStaffList} />
            <Route path="/departmentSchedule" exact component={departmentSchedule} />
            <Route path="/employeeSchedule" exact component={employeeSchedule} />
            <Route path="/phonebook" exact component={phonebook} />
            <Route path="/employeeDetails" exact component={employeeDetails} />
            <Route path="/" exact component={HomePage} />
            <Route component={notFound} />
          </Switch >
        </Layout>
    </div>
  );
}

export default App;
