import React, { Component } from 'react';

import styles from './Layout.module.css';
import SideDrawer from '../components/Navigation/SideDrawer/SideDrawer'
import Toolbar from '../components/Navigation/Toolbar/Toolbar';
import Footer from '../components/Footer/Footer'


class Layout extends Component {
    state = {
        showSideDrawer: false
    }

    sideDrawerClosedHandler = () => {
        this.setState( { showSideDrawer: false } );
    }

    sideDrawerToggleHandler = () => {
        this.setState( ( prevState ) => {
            return { showSideDrawer: !prevState.showSideDrawer };
        } );
    }

    render () {
        return (
            <React.Fragment>
                <Toolbar drawerToggleClicked={this.sideDrawerToggleHandler}/>
                <SideDrawer
                    open={this.state.showSideDrawer}
                    closed={this.sideDrawerClosedHandler} />
                <main className={styles.Content}>
                    {this.props.children}
                </main>
                <Footer/>
            </React.Fragment>
        )
    }
}

export default Layout;