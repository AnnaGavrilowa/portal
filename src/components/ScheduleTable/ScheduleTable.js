import React from 'react';

import ScheduleTableItem from './ScheduleTableItem/ScheduleTableItem'

const scheduleTable = ( props ) => {

    return (
        <div>
            <ScheduleTableItem date={props.date} scheduleData={props.scheduleData} ifInGrid={props.ifInGrid} filterWeeks={props.filterWeeks} filterSubgroups={props.filterSubgroups}/>
        </div>
    )
};
export default scheduleTable;