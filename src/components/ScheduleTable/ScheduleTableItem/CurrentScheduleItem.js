import React from 'react';
import {OverlayTrigger, Tooltip} from 'react-bootstrap'
import styles from './ScheduleTableItem.module.css';
import {calculateEndOfLesson, setGroups} from '../../../services/scheduleService'

const scheduleTableItem = ( props ) => {
    let scheduleTableItemList = []
    if(props.scheduleData.length !== 0) {
        for (const [index, value] of props.scheduleData.entries()) {
            let subjectsList = []

            let groupNumbersList = []
            if(value.studentGroupInformation)
                for (const [indexOfGroup, valueOfGroup] of setGroups(value.studentGroupInformation).entries()) {

                    const renderTooltip = (props) => (
                        <Tooltip id="button-tooltip" className={`tooltip-primary`} {...props} >
                            {valueOfGroup.code + ' (' + valueOfGroup.studentAmount + ' студентов)'}
                        </Tooltip>
                    );

                    groupNumbersList.push(<div key={indexOfGroup}>
                        <OverlayTrigger
                            placement="right"
                            delay={{ show: 100, hide: 100 }}
                            overlay={renderTooltip}>
                            <a 
                                href
                                className={styles.groupNumber}>
                                    {valueOfGroup.groupNumber}
                            </a>
                        </OverlayTrigger>
                    </div>)
            }

            let auditoryList = []
            if(value.auditory)
                for (const [indexOfAuditory, valueOfAuditory] of value.auditory.entries()) {
                    auditoryList.push(<div key={indexOfAuditory} className={styles.location}> 
                            {valueOfAuditory}
                        </div>)
            }
            
            let dataContainerStyle = styles.infoHolder
            if (value.note) dataContainerStyle = styles.infoHolderWithNote

            subjectsList.push(<div                
                className={dataContainerStyle} 
                key={index}>
                    <div className={styles.item}>
                        <span className={styles.startTime}>{value.startLessonTime}</span><br/>
                        <span className={styles.endTime}>{calculateEndOfLesson(value.startLessonTime)}</span>
                    </div>
                    <div className={styles.location}>{auditoryList}</div>
                    {(value.numSubgroup !== 0) ? <div className={styles.subgroup}>{value.numSubgroup + ' п.'}</div> : null}    
                        <div className={styles.subjectName}>{value.subject + ' (' + value.lessonType + ')' }</div>
                        {groupNumbersList}
                    <div className={styles.note}>{value.note}</div>                          
                </div>)
            
            scheduleTableItemList.push(<div 
                key={index}
                className={styles.scheduleItemHolder}>
                    {subjectsList}
                </div>)
        }
    }
    else {
        scheduleTableItemList.push(<div className={styles.scheduleItemHolder}>
            <div className={styles.noLessons}>Занятий нет</div>
        </div>)
    }

    return (
        <React.Fragment>
            <div className={styles.dateHolder}>{props.date}</div>
            {scheduleTableItemList}
        </React.Fragment>
    )
};
export default scheduleTableItem;