import React from 'react';
import {OverlayTrigger, Tooltip} from 'react-bootstrap'
import styles from './ScheduleTableItem.module.css';
import {calculateEndOfLesson, setGroups, filterScheduleByWeek, filterScheduleBySubgroup} from '../../../services/scheduleService'

const scheduleTableItem = ( props ) => {

    let scheduleTableItemList = []
    if(props.scheduleData && props.scheduleData.length !== 0) {
        let filteredSchedule1 = filterScheduleByWeek(props.scheduleData, props.filterWeeks)
        let filteredSchedule2 = filterScheduleBySubgroup(filteredSchedule1, props.filterSubgroups)
        for (const [index, value] of filteredSchedule2.entries()) {
            let subjectsList = []
            if(value.schedule.length !== 0)
                for (const [indexOfSubject, valueOfSubject] of value.schedule.entries()) {

                    let weekNumbersList = []
                    if(valueOfSubject.weekNumber)
                        for (const [indexOfWeek, valueOfWeek] of valueOfSubject.weekNumber.entries()) {
                            weekNumbersList.push(<span 
                                key={indexOfWeek}
                                className={styles.weekNumber}>
                                    {valueOfWeek}
                                </span>)
                    }

                    let groupNumbersList = []
                    if(valueOfSubject.studentGroupInformation)
                        for (const [indexOfGroup, valueOfGroup] of setGroups(valueOfSubject.studentGroupInformation).entries()) {

                            const renderTooltip = (props) => (
                                <Tooltip id="button-tooltip" className={`tooltip-secondary`} {...props} >
                                  {valueOfGroup.code + ' (' + valueOfGroup.studentAmount + ' студентов)'}
                                </Tooltip>
                            );

                            groupNumbersList.push(<div key={indexOfGroup}>
                                <OverlayTrigger
                                    placement="right"
                                    delay={{ show: 100, hide: 100 }}
                                    overlay={renderTooltip}>
                                    <a 
                                        href
                                        className={styles.groupNumber}>
                                            {valueOfGroup.groupNumber}
                                    </a>
                                </OverlayTrigger>
                            </div>)
                    }

                    let auditoryList = []
                    if(valueOfSubject.auditory)
                        for (const [indexOfAuditory, valueOfAuditory] of valueOfSubject.auditory.entries()) {
                            auditoryList.push(<div key={indexOfAuditory} className={styles.location}> 
                                    {valueOfAuditory}
                                </div>)
                    }

                    let subjectStyle = ''
                    if (props.ifInGrid === "true" && weekNumbersList.length !== 5) subjectStyle = styles.subjectName
                    else subjectStyle = styles.subjectNameMargin

                    subjectsList.push(<div 
                        className={styles.infoHolder} 
                        key={indexOfSubject}>
                            <div className={styles.item}>
                                <span className={styles.startTime}>{valueOfSubject.startLessonTime}</span><br/>
                                <span className={styles.endTime}>{calculateEndOfLesson(valueOfSubject.startLessonTime)}</span>
                            </div>
                            {(props.ifInGrid === "true" && weekNumbersList.length !== 5) ? <div className={styles.weekNumberHolder}>
                                {weekNumbersList}
                            </div> : <div className={styles.weekNumberHolder}></div>}
                            <div className={styles.location}>{auditoryList}</div>
                            {(valueOfSubject.numSubgroup !== 0) ? <div className={styles.subgroup}>{valueOfSubject.numSubgroup + ' п.'}</div> : null}    
                                <div className={subjectStyle}>{valueOfSubject.subject + ' (' + valueOfSubject.lessonType + ')' }</div>
                                {groupNumbersList}
                            <div className={styles.note}>{valueOfSubject.note}</div>                          
                        </div>)
                }
            
            scheduleTableItemList.push(<div 
                key={index}
                className={styles.scheduleItemHolder}>
                    {(props.ifInGrid === "true" && value.schedule.length !== 0) ? <div className={styles.dateHolder}>
                        {value.weekDay}
                    </div> : null                  
                    }
                    {subjectsList}
                </div>)
        }
    }
    return (
        <React.Fragment>
            {scheduleTableItemList}
        </React.Fragment>
    )
};
export default scheduleTableItem;