import React, { Component } from 'react';
import { connect } from 'react-redux';
 import {Alert } from "react-bootstrap";
import { getTerm, getCurrentEducationalYear, isScheduleAvailable } from '../../services/scheduleService'
import { Typeahead } from 'react-bootstrap-typeahead';
import excel from '../../assets/excel.png'
import styles from './DepartmentSchedule.module.css';
import * as actions from '../../store/actions/index';

import Spinner from '../UI/Spinner/Spinner'

class departmentSchedule extends Component {

    state = {
        selectedDepartment: null,
    }

    componentDidMount() {
        this.props.onGetWeek()
        this.props.onGetDepartments()
    }

    render() {

        if(isScheduleAvailable) {
        let downloadButton = <input disabled type="image" src={excel} alt="Excel logo" className={styles.downloadExcelButton}/>
        if (this.state.selectedDepartment !== null && this.props.loading === true) downloadButton = <Spinner />
        if (this.state.selectedDepartment !== null && this.props.loading === false) 
            downloadButton = <input 
                type="image" 
                src={excel} 
                alt="Excel logo" 
                className={styles.downloadExcelButton} 
                onClick={() => this.props.onGetDepartmentScheduleDownload(this.state.selectedDepartment[0].idDepartment,
                                                                          'https://journal.bsuir.by/api/v1/portal/departmentSchedule/report?departmentId=',
                                                                          'Расписание кафедры ' + this.state.selectedDepartment[0].abbrev,
                                                                          'd')}/>

       return (
        <div className={styles.departmentScheduleHolder}>
            <h2 className={styles.heading}>РАСПИСАНИЕ КАФЕДРЫ</h2>
            <h4 className={styles.yearHolder}>{getTerm()+' семестр, ' + getCurrentEducationalYear() + ' - ' + (getCurrentEducationalYear()+1)+' учебный год'}</h4>
            <h4 className={styles.weekHolder}>Сейчас {this.props.weekNumber} учебная неделя</h4>
            <Typeahead
                id="departmentScheduleSearch"
                className={styles.searchHolder}
                minLength={1}
                emptyLabel="Совпадения не найдены"
                labelKey="abbrev"
                options={this.props.departments}
                placeholder="Введите название кафедры..."
                onChange={(s) => {
                        if (s.length !== 0)
                            this.setState(state => {return {...state, selectedDepartment: s}})
                    }
                }
            />
            {downloadButton}
        </div>
    )} else {
        return (
            <Alert className={styles.alertHolder} variant='danger'>
                                На данный момент расписание недоступно,<br/>
                                идёт формирование расписания на следующий семестр. <br/>
                                Расписание будет доступно после 15 августа.
            </Alert>
        )
    }
}
};

const mapStateToProps = state => {
    return {
        departments: state.departments.departments,
        loading: state.schedule.loading,
        weekNumber: state.schedule.weekNumber
    };
};

const mapDispatchToProps = dispatch => {
    return {
        onGetWeek: () => dispatch (actions.getWeekNumber()),
        onGetDepartments: () => dispatch( actions.getDepartments() ),
        onGetDepartmentScheduleDownload: (id, url, name, targetDownload) => dispatch (actions.getScheduleDownload(id,url,name,targetDownload))
    };
};

export default connect(mapStateToProps,mapDispatchToProps)(departmentSchedule);