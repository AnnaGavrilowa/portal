import React, {Component} from 'react';
import { connect } from 'react-redux';
import { Modal } from "react-bootstrap";
import styles from './FilterSchedule.module.css';
import * as actions from '../../store/actions/index';
import excel from '../../assets/excel.png'
import googleCalendar from '../../assets/googleCalendarIcon.png'

class filterSchedule extends Component {

    state = {
        ifShowModal: false,
        ifCheckedWeek: [true,true,true,true],
        ifCheckedSubgroup: [true,true]
    }

    componentDidMount() {
        this.props.onGetWeek()
    }

    handleShow = () => {
        this.setState(state => {return {...state, ifShowModal: true}})
    }

    handleClose = () => {
        this.setState(state => {return {...state, ifShowModal: false}})
    }

    handleChangeWeek = (event) => {
        let ifCheckedWeek = this.state.ifCheckedWeek
        ifCheckedWeek[event.target.value-1] = event.target.checked
        this.setState(state => {return {...state, ifCheckedWeek: ifCheckedWeek}})
        this.props.onSetCheckedWeeks(ifCheckedWeek)
    }

    handleChangeSubgroup = (event) => {
        let ifCheckedSubgroup = this.state.ifCheckedSubgroup
        ifCheckedSubgroup[event.target.value-1] = event.target.checked
        this.setState(state => {return {...state, ifCheckedSubgroup: ifCheckedSubgroup}})
        this.props.onSetCheckedSubgroups(ifCheckedSubgroup)
    }

    selectAll = () => {
        this.setState(state => {return {...state, ifCheckedWeek: [true,true,true,true]}})
        this.props.onSetCheckedWeeks([true,true,true,true])
    }

    render() {
        let styleModal = styles.modalHolder

        return (
            <div>
                <div className={styles.heading}>Фильтры</div>
                <div className={styles.weekNumberHolder}>Недели (сейчас {this.props.weekNumber} неделя)</div>
                <div className={styles.checkboxWeekHolder}>
                    <input type="checkbox" checked={this.state.ifCheckedWeek[0]} value={1} id="checkWeek1" onChange={this.handleChangeWeek}/><label htmlFor ="checkWeek1" className={styles.checkLabel}>1</label>
                    <input type="checkbox" checked={this.state.ifCheckedWeek[1]} value={2} id="checkWeek2" onChange={this.handleChangeWeek}/><label htmlFor ="checkWeek2" className={styles.checkLabel}>2</label>
                    <input type="checkbox" checked={this.state.ifCheckedWeek[2]} value={3} id="checkWeek3" onChange={this.handleChangeWeek}/><label htmlFor ="checkWeek3" className={styles.checkLabel}>3</label>
                    <input type="checkbox" checked={this.state.ifCheckedWeek[3]} value={4} id="checkWeek4" onChange={this.handleChangeWeek}/><label htmlFor ="checkWeek4" className={styles.checkLabel}>4</label>
                    <button className={styles.selectAllCheckboxesButton} onClick={this.selectAll}>все</button>
                </div>
                <div className={styles.subgroupHolder}>Подгруппы</div>
                <div className={styles.checkboxSubgroupHolder}>
                    <input type="checkbox" checked={this.state.ifCheckedSubgroup[0]} value={1} id="checkSubgroup1" onChange={this.handleChangeSubgroup}/><label htmlFor ="checkSubgroup1" className={styles.checkLabel}>1</label>
                    <input type="checkbox" checked={this.state.ifCheckedSubgroup[1]} value={2} id="checkSubgroup2" onChange={this.handleChangeSubgroup}/><label htmlFor ="checkSubgroup2" className={styles.checkLabel}>2</label>
                </div>
                <div className={styles.infoHeading}>Информация</div>
                <div className={styles.buttonsHolder}>
                    <input 
                        type="image" 
                        src={excel} 
                        alt="Excel logo" 
                        className={styles.downloadButton} 
                        onClick={() => this.props.onGetScheduleDownload(this.props.id,
                                                                        'https://journal.bsuir.by/api/v1/portal/employeeSchedule/report?employeeId=',
                                                                        'Расписание ' + this.props.fio,
                                                                        'e')}/>
                    <input onClick={this.handleShow} type="image" src={googleCalendar} alt="Google calendar logo" className={styles.downloadButton}/>
                    
                    <Modal show={this.state.ifShowModal} onHide={this.handleClose} dialogClassName={styleModal}>
                        <Modal.Header closeButton className={styles.modalHeader}>
                            <Modal.Title>Импорт Google календаря</Modal.Title>
                        </Modal.Header>
                        <Modal.Body>
                            <div className={styles.modalDataHeading}>Информация о Google календаре</div>
                            <ul>
                                <li>мы предоставляем календарь с расписанием, который полностью синхронизируется на основе наших данных.</li>
                                <li>после импорта календаря на вашу учетную запись вы будете иметь доступ к актуальной информации на вашем телефоне в любом приложении, которое поддерживает синхронизацию с Google аккаунтом.</li>
                            </ul>
                            <div className={styles.modalDataHeading}>Импортировать себе календарь "{this.props.fio}"</div>
                            <ul>
                                <li>по <a target="_blank" rel="noopener noreferrer" href={`https://calendar.google.com/calendar/render?cid=${this.props.calendarId}`}>ссылке</a></li>
                                <li>используя id календаря <span className={styles.calendarIdHolder}>{this.props.calendarId}</span></li>
                            </ul>
                            <div className={styles.modalDataHeading}>Как использовать</div>
                            <div>К примеру вы можете воспользоваться официальным приложением от <a target="_blank" rel="noopener noreferrer" href="https://play.google.com/store/apps/details?id=com.google.android.calendar">Google</a>.</div>
                            <div>Справочную информацию по календарям вы можете найти <a target="_blank" rel="noopener noreferrer" href="https://support.google.com/calendar/answer/6261951?hl=ru&ref_topic=3417927">здесь</a>.</div>
                        </Modal.Body>
                    </Modal>
                </div>
            </div>
        )
    }
};

const mapStateToProps = state => {
    return {
        weekNumber: state.schedule.weekNumber,
        employeeSchedule: state.schedule.employeeSchedule,
        checkedWeeks: state.schedule.checkedWeeks,
        checkedSubgroups: state.schedule.checkedSubgroups
    };
};

const mapDispatchToProps = dispatch => {
    return {
        onGetWeek: () => dispatch (actions.getWeekNumber()),
        onGetScheduleDownload: (id, url, name, targetDownload) => dispatch (actions.getScheduleDownload(id,url,name,targetDownload)),
        onSetCheckedWeeks: (value) => dispatch (actions.setCheckedWeeks(value)),
        onSetCheckedSubgroups: (value) => dispatch (actions.setCheckedSubgroups(value))
    };
};

export default connect(mapStateToProps,mapDispatchToProps)(filterSchedule);