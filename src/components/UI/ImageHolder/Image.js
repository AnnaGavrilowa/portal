import React, { Component } from 'react';
import styles from './Image.module.css'
import LazyLoad from 'react-lazy-load';

class Image extends Component {
    constructor() {
      super();
      this.state = {};
      this.fallback = () => {
        if (this.props.fallbackSrc) {
          this.setState({ failed: true });
        }
      };
    }
  
    render() {

      let cName = ''
      if (this.props.cName === 'employeeIcon') cName = styles.employeeIcon

      if (this.state.failed) {
        return (
          <LazyLoad className={ (cName !== '') ? styles.lazyloadHolder : null}
            width={this.props.size}
            height={this.props.size}
            debounce={false}
          >
            <img className={cName} src={this.props.fallbackSrc} alt="Default Icon" width="100%" height="100%" />
          </LazyLoad>
          )
      } else {
        return (
          <LazyLoad className={ (cName !== '') ? styles.lazyloadHolder : null}
            width={this.props.size}
            height={this.props.size}
            debounce={false}
          >
            <img className={cName} src={this.props.src} onError={this.fallback} width="100%" height="100%" alt="Profile Icon"/>
          </LazyLoad>
        )
      }
    }
  }

export default Image