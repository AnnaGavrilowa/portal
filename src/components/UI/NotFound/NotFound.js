import React from 'react';
import BlackPanda from '../../../assets/black_panda.jpeg'
import styles from './NotFound.module.css';

const notFound = ( props ) => {
        return (
            <div className={styles.container}>
                <div className={styles.imageHolder}><img src={BlackPanda} alt="404"/></div>
                <div className={styles.message}>Страница не найдена</div>
            </div>)
};

export default notFound;