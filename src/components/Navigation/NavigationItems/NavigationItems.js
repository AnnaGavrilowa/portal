import React from 'react';

import styles from './NavigationItems.module.css';

import NavigationItem from './NavigationItem/NavigationItem';
import Logo from '../../Logo/Logo';
import DefaultIcon from './../../../assets/default-photo.png'

const scheduleDropdownList = [
    {   path: '/groupSchedule',
        name: 'Расписание группы'},
    {   path: '/employeeSchedule',
        name: 'Расписание преподавателя'},
    {   path: '/departmentSchedule',
        name: 'Расписание кафедры'},
    {   path: '/announcements',
        name: 'Объявления кафедры'}
]
const departmentDropdownList = [
    {   path: '/departments',
        name: 'Сотрудники кафедры'}
]

const navigationItems = () => {
    return (
        <ul className={styles.NavigationItems}>
            <div className={styles.Logo}>
                <a href="/"><Logo /></a>
            </div>
            <div className={styles.desktopNav}><NavigationItem link="/announcements" exact = "true" dropdownElements={scheduleDropdownList} >Расписания</NavigationItem></div>
            <div className={styles.desktopNav}><NavigationItem link="/departments" dropdownElements={departmentDropdownList} >Кафедры</NavigationItem></div>
            <div className={styles.mobileNav}><NavigationItem link="/groupSchedule" >Расписание группы</NavigationItem></div>
            <div className={styles.mobileNav}><NavigationItem link="/employeeSchedule" >Расписание преподавателя</NavigationItem></div>
            <div className={styles.mobileNav}><NavigationItem link="/departmentSchedule" >Расписание кафедры</NavigationItem></div>
            <div className={styles.mobileNav}><NavigationItem link="/announcements" >Объявления кафедры</NavigationItem></div>
            <NavigationItem link="/students">Студенты</NavigationItem>
            <NavigationItem link="/rating">Рейтинг</NavigationItem>
            <NavigationItem link="/disciplines">Перечень дисциплин</NavigationItem>
            <NavigationItem link="/phonebook">Телефонный справочник</NavigationItem>
            <div className={styles.signinButton} ><NavigationItem link="/signin">
                {/* раскомментить, когда будет проверка на isAuth */}
                {/* <img src={DefaultIcon}
                alt="Default Icon"
                className={styles.accountAvatar}/>
                <span> Мой аккаунт</span> */}
                <span>Войти</span>
            </NavigationItem></div>
        </ul>
    )
};

export default navigationItems;