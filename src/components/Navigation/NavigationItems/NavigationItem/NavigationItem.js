import React from 'react';
import { Link } from 'react-router-dom';

import styles from './NavigationItem.module.css';

const navigationItem = ( props ) => {
    let dropdownElementsListJSX = []
    if(props.dropdownElements)
        for (const [index, value] of props.dropdownElements.entries()) {
            dropdownElementsListJSX.push(<Link 
                key={index} 
                exact="true" 
                to={value.path}
                className={styles.dropLink}>{value.name}</Link>)
        }
    let ifDropDown = false;
    if (dropdownElementsListJSX.length !== 0) {
        ifDropDown = true
    }

    let navLink = (<a 
        href={props.link.toString()}
        exact={props.exact}
        
        >{props.children}</a>);
    if (ifDropDown) {
        navLink = (<a 
            href
            exact={props.exact}
            className = {styles.linkWithDropdown}
            >{props.children}</a>);
    }

    return (
     <li className={styles.NavigationItem}> 
        <div className={styles.dropdown}>
            {navLink}
            {ifDropDown ? <div className={styles.dropdownContent}>               
                {dropdownElementsListJSX}
            </div> : null}
        </div>
    </li>);
};

export default navigationItem;