import React, {Component} from 'react';

import styles from './Toolbar.module.css';
import DrawerToggle from '../SideDrawer/DrawerToggle/DrawerToggle';
import NavigationItems from '../NavigationItems/NavigationItems';
import Logo from '../../Logo/Logo';
import {withRouter } from 'react-router-dom';

class toolbar extends Component{
    constructor(props) {
        super(props);
    }

    render() {
        return (
        <header className={styles.Toolbar}>
            <div className={styles.Logo}>
                <Logo clicked={() => this.props.history.push("/")} size="40px"/>
            </div>
            <DrawerToggle clicked={this.props.drawerToggleClicked} />
            <nav className={styles.DesktopOnly}>
                <NavigationItems />
            </nav>
        </header>)
    }
};
export default withRouter(toolbar);