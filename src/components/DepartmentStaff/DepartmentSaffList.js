import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Typeahead } from 'react-bootstrap-typeahead';
import styles from './DepartmentStaffList.module.css';

import * as actions from '../../store/actions/index';

import DepartmentStaffItem from './DepartmentStaffItem/DepartmentStaffItem'
import Spinner from '../UI/Spinner/Spinner'

class departmentStaffList extends Component {

    state = {
        selectedDepartment: [],
    }

    componentDidMount() {
        this.props.onGetDepartments()
        // if(localStorage.getItem('selectedDepartment') !== null) {
        //     let retrievedObject = localStorage.getItem('selectedDepartment');
        //     let parsedObject = []
        //     parsedObject.push(JSON.parse(retrievedObject))
        //     this.state.selectedDepartment = parsedObject
        //     //this.setState(state => {return {...state, selectedDepartment: parsedObject}})
        //     this.props.onGetDepartmentStaff(this.state.selectedDepartment[0].idDepartment)
        // }
    }

    render() {

        let departmentStaffTable
        if (this.state.selectedDepartment === null) departmentStaffTable = null
        if (this.state.selectedDepartment.length !== 0 && this.props.loading === true) departmentStaffTable = <Spinner />
        if (this.state.selectedDepartment.length !== 0  && this.props.loading === false) departmentStaffTable = <DepartmentStaffItem employeeData={this.props.departmentStaff.departmentStaffData}/>
        
        return (<div className={styles.departmentStaffListHolder}>
            <h2 className={styles.heading}>СОТРУДНИКИ КАФЕДРЫ</h2>
            <Typeahead
                className={styles.searchHolder}
                minLength={0}
                emptyLabel="Совпадения не найдены"
                id="basic-behaviors-example"
                labelKey="abbrev"
                options={this.props.departments}
                selected={this.state.selectedDepartment}
                placeholder="Введите название кафедры"
                onChange={(s) => {
                        localStorage.removeItem('selectedDepartment')
                        if (s.length !== 0) {
                            this.props.onGetDepartmentStaff(s[0].idDepartment)
                            
                            localStorage.setItem('selectedDepartment', JSON.stringify(s[0]));
                        } else {
                            //clear LS
                        }
                        this.setState(state => {return {...state, selectedDepartment: s}})
                    }
                }
        />
        {departmentStaffTable}
    </div>)}
};

const mapStateToProps = state => {
    return {
        departments: state.departments.departments,
        departmentStaff: state.departments.departmentStaff,
        loading: state.departments.loading
    };
};

const mapDispatchToProps = dispatch => {
    return {
        onGetDepartments: () => dispatch( actions.getDepartments() ),
        onGetDepartmentStaff: (id) => dispatch (actions.getDepartmentStaff(id))
    };
};


export default connect(mapStateToProps,mapDispatchToProps)(departmentStaffList);