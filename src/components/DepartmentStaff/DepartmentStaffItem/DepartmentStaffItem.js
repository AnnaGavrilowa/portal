import React from 'react';

import styles from './DepartmentStaffItem.module.css';

import DefaultIcon from '../../../assets/default-photo.png'
import Image from '../../UI/ImageHolder/Image'
import {phoneNumberModification} from '../../../services/generalService'

const departmentStaffList = ( props ) => {
    
    let employeeList = []
    if(props.employeeData)
    
        for (const [index, value] of props.employeeData.entries()) {

            let hrefEmail
            if (value.email) hrefEmail = 'mailto:' + value.email

            let jobPositionList = []
            if (value.jobPositions) {
                for (const [jobIndex, jobValue] of value.jobPositions.entries()) {
                    
                    let contactDataList = []
                    if (jobValue.contacts) {
        
                        if (jobValue.contacts.length > 1)
                            for (const [indexContactData, valueContactData] of jobValue.contacts.entries()) {
                                contactDataList.push(
                                        <li key={indexContactData}>
                                            {valueContactData.phoneNumber ? <div>Рабочий телефон: <a href={'tel:' + valueContactData.phoneNumber}>{phoneNumberModification(valueContactData.phoneNumber)}</a></div> : null }
                                            {valueContactData.address ? <div>{valueContactData.address}</div> : null }
                                        </li>)
                            }
        
                        if (jobValue.contacts.length === 1) 
                            for (const [indexContactData, valueContactData] of jobValue.contacts.entries()) {
                                contactDataList.push(
                                        <div key={indexContactData}>
                                            {valueContactData.phoneNumber ? <div>Рабочий телефон: <a href={'tel:' + valueContactData.phoneNumber}>{phoneNumberModification(valueContactData.phoneNumber)}</a></div> : null }
                                            {valueContactData.address ? <div>{valueContactData.address}</div> : null }
                                        </div>)
                            }
                    }

                    jobPositionList.push(
                        <div key={jobIndex}>
                            {(jobValue.jobPosition) ? <div>Должность: {jobValue.jobPosition}</div> : null}
                            <div>Место работы: {jobValue.department}</div>
                            { (jobValue.contacts && jobValue.contacts.length > 1) ? <ul>{contactDataList}</ul> : <div>{contactDataList}</div>}
                        </div>
                    )
                }
            }


            employeeList.push(
                <div key={index} className={styles.personalCardHolder}>
                    <Image size={160} src={value.photoLink} fallbackSrc={DefaultIcon} />
                    <div className={styles.personalDataHolder}>
                        <h4 className={styles.fullNameHeader}>{value.lastName + ' ' + value.firstName + ' ' + value.middleName}</h4>
                        { value.degree ? <div>{value.degree}</div> : null }     
                        { value.email ? <div>Email: <a href={hrefEmail.toString()}>{value.email}</a></div> : null }     
                        {/* { value.position ? <div>Должность: {value.position}</div> : null }     
                        { value.department ? <div>Место работы: {value.department}</div> : null }   */}
                        {jobPositionList}
                    </div>                    
                </div>)
            
        }

    return(
        <React.Fragment>
            {employeeList}
        </React.Fragment>
        
    );
};
export default departmentStaffList;