import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Table, Tooltip, OverlayTrigger, Alert } from "react-bootstrap";
import styles from './Phonebook.module.css';
import Image from '../UI/ImageHolder/Image'
import * as actions from '../../store/actions/index';
import {phoneNumberModification} from '../../services/generalService'
import Spinner from '../UI/Spinner/Spinner'
import DefaultIcon from '../../assets/default-photo.png'

class phonebook extends Component {

    state = {
        searchingValue: '',
        startSearching: true
    }

    handleKeyDown = (e) => {
        if (e.key === 'Enter') {
            this.setState(state => {return {...state, searchingValue: e.target.value.trim(), startSearching: false}})
            this.props.onGetPhonebookData(e.target.value.trim())           
        }
    }

    handleChange(event) {
            this.setState(state => {return {...state, searchingValue: event.target.value.trim()}})
    }

    newSearch = (value) => {
        this.setState(state => {return {...state, searchingValue: value.trim()}})
        this.props.onGetPhonebookData(value.trim())
    }

    render() {      

        let phoneTable = null
        let errorSearch = null
        if (this.state.searchingValue !== '' && this.props.loading === true) phoneTable = <Spinner/>
        if (this.state.searchingValue !== '' && this.props.loading === false) {
            let tableDataArray = []
            if (this.props.phonebookData.length !== 0) {
                for (const [index, value] of this.props.phonebookData.entries()) {

                    let phoneNumberList = []
                    if(value.phones)
                        for (const [indexOfPhone, valueOfPhone] of value.phones.entries()) {
                            phoneNumberList.push(<div key={indexOfPhone}><a href={'tel:'+ valueOfPhone} className={styles.clickable}> 
                                    {phoneNumberModification(valueOfPhone)}
                                </a></div>)
                    }

                    let employeesArray = []
                    for (const [indexEmployee, valueEmployee] of value.employees.entries()) {

                        const renderTooltip = (props) => (
                            <Tooltip id="button-tooltip" className={`tooltip-secondary`} {...props} >
                            <React.Fragment>
                                <Image size={100} cName="employeeIcon" src={valueEmployee.photoLink} fallbackSrc={DefaultIcon} />
                            </React.Fragment>
                            </Tooltip>
                        );
                        employeesArray.push(
                            <div key={indexEmployee} className={styles.employeeHolder}>
                                <OverlayTrigger
                                        placement="top"
                                        delay={{ show: 100, hide: 100 }}
                                        overlay={renderTooltip}>
                                    <div className={styles.clickable} 
                                         onClick={()=>{this.props.history.push({
                                             search: '?query='+valueEmployee.id,
                                             pathname: "/employeeDetails"})}}>
                                             {valueEmployee.fio}
                                    </div>
                                </OverlayTrigger>
                                <div>
                                    {(valueEmployee.jobPosition !== '') ? <span>{valueEmployee.jobPosition + ', '}</span> : null}
                                    {(valueEmployee.department !== '') ? <span>{valueEmployee.department + ', '}</span> : null}
                                    {(valueEmployee.degree !== '') ? <span>{valueEmployee.degree + ', '}</span> : null}
                                    {(valueEmployee.rank !== 'без звания') ? <span>{valueEmployee.rank}</span> : null}
                                </div>
                                <div>Email: <a className={styles.clickable} href={'mailto:'+valueEmployee.email}>{valueEmployee.email}</a></div>
                            </div>
                        )
                    }

                    tableDataArray.push(
                        <tr key={index}>
                            <td>{phoneNumberList}</td>
                            <td>
                                <div onClick={() => this.newSearch(value.auditory)} className={styles.clickable}>{value.auditory}</div>
                                <div>{value.buildingAddress}</div>
                            </td>
                            <td>
                                <div onClick={() => this.newSearch(value.auditory)} className={styles.clickable}>{value.departmentAbbrev}</div>
                                <div className={styles.departmentContent}>{value.departmentName}</div>
                            </td>
                            <td>{employeesArray}</td>
                            <td>{value.note}</td>
                        </tr>
                    )
                }

                phoneTable = 
                    <Table bordered hover className={styles.tablePhonebook}>
                        <thead>
                            <tr>
                                <th width="20%">Номер телефона</th>
                                <th width="17%">Помещение</th>
                                <th width="20%">Подразделение</th>
                                <th width="26%">Сотрудники</th>
                                <th width="17%">Примечание</th>
                            </tr>
                        </thead>
                        <tbody>
                            {tableDataArray}
                        </tbody>
                    </Table>
            } else {
                errorSearch = <Alert className={styles.alertHolder} variant='danger'>Результаты не найдены.</Alert>
            }
        }

        return (
            <div className={styles.container}>
                <h2 className={styles.heading}>ТЕЛЕФОННЫЙ СПРАВОЧНИК</h2>
                {/* при поиске по номеру телефона проверять все форматы и обрезать лишнее, если, например, ввели номер со скобками или тире  */}
                <input value={this.state.searchingValue} type="text" className={styles.searchHolder} placeholder="Поиск..." onKeyDown={this.handleKeyDown} onChange={(e)=>this.handleChange(e)}/>
                {(this.props.phonebookData.length !== 0) ? <div>{phoneTable}</div> : (!this.state.startSearching) ? <div>{errorSearch}</div> : null}
                <div className={styles.support}>Справочная АТС университета: <a href="tel:+375172938909">+375 (17) 293-89-09</a></div>
            </div>
        )
    }
};

const mapStateToProps = state => {
    return {
        phonebookData: state.phonebook.phonebookData,
        loading: state.phonebook.loading
    };
};

const mapDispatchToProps = dispatch => {
    return {
        onGetPhonebookData: (value) => dispatch(actions.getPhonebookData(value)),
    };
};

export default connect(mapStateToProps,mapDispatchToProps)(phonebook);