import React, {Component} from 'react';
import { connect } from 'react-redux';
import styles from './EmployeeSchedule.module.css';
import { Tab, Tabs, Alert } from "react-bootstrap";
import { AsyncTypeahead } from 'react-bootstrap-typeahead';
import * as actions from '../../store/actions/index';
import { getTerm, getCurrentEducationalYear, isScheduleAvailable } from '../../services/scheduleService'
import DefaultIcon from '../../assets/default-photo.png' 
import ScheduleTable from '../ScheduleTable/ScheduleTable'
import FilterSchedule from '../FilterSchedule/FilterSchedule'
import CurrentScheduleItem from '../ScheduleTable/ScheduleTableItem/CurrentScheduleItem'
import Spinner from '../UI/Spinner/Spinner'
import Image from '../UI/ImageHolder/Image'
import {phoneNumberModification} from '../../services/generalService'

class employeeSchedule extends Component {
    
    state = {
        selectedEmployee: null,
    }

    componentDidMount() {
        this.props.onGetWeek()
        //this.props.onGetEmployees()
    }
    
    render() {
        if (!isScheduleAvailable()) {

            let employeeSchedule
            if (this.state.selectedEmployee === null) employeeSchedule = null
            if (this.state.selectedEmployee !== null && this.props.loading === true) employeeSchedule = <Spinner />
            if (this.state.selectedEmployee !== null && this.props.loading === false) {
            
            let employeeAnnouncementsList = []
            if(this.props.employeeAnnouncements)
                for (const [indexA, valueA] of this.props.employeeAnnouncements.employeeAnnouncementsData.entries()) {
                    employeeAnnouncementsList.push(<div key={indexA} className={styles.infoBodyAnnouncements}> 
                        <div className={styles.announcementData}>{valueA.date}</div>
                        <div className={styles.announcementContent}>{valueA.content}</div>
                    </div>)
            }

            let announcements = <div className={styles.infoBody}>Объявлений нет</div>
            if (this.props.employeeAnnouncements)
                if (this.props.employeeAnnouncements.employeeAnnouncementsData.length !== 0)
                    announcements = <div>{employeeAnnouncementsList}</div>

                employeeSchedule = <div>
                    <div>
                        <div className={styles.clickable} onClick={()=>{this.props.history.push({
                                             search: '?query='+this.props.employeeSchedule.employee.id,
                                             pathname: "/employeeDetails"})}}>
                            <Image size={120} cName="employeeIcon" src={this.props.employeeSchedule.employee.photoLink} fallbackSrc={DefaultIcon} />
                            <div className={styles.fullName}>{this.props.employeeSchedule.employee.lastName + ' ' + this.props.employeeSchedule.employee.firstName + ' ' + this.props.employeeSchedule.employee.middleName}</div>
                        </div>
                        <div className={styles.lessonsPeriod}>
                            <div className={styles.floatLeft}>
                                <div>Начало занятий: {this.props.employeeSchedule.dateStart}</div>
                                <div>Конец занятий: {this.props.employeeSchedule.dateEnd}</div>
                            </div>
                            {(this.props.employeeSchedule.sessionStart !== null || this.props.employeeSchedule.sessionEnd !== null) ? <div className={styles.floatRight}>
                                <div>Начало сессии: {this.props.employeeSchedule.sessionStart}</div>
                                <div>Конец сессии: {this.props.employeeSchedule.sessionEnd}</div>
                            </div> : null}
                        </div>
                    </div>
                    <div className={styles.actualInfoHolder}>
                        <div className={styles.actualInfoBlock}>
                            <CurrentScheduleItem ifInGrid="false" date={this.props.employeeSchedule.todayDate} scheduleData={this.props.employeeSchedule.todaySchedules}/>
                        </div>
                        <div className={styles.actualInfoBlock}>
                            <CurrentScheduleItem ifInGrid="false" date={this.props.employeeSchedule.tomorrowDate} scheduleData={this.props.employeeSchedule.tomorrowSchedules}/>
                        </div>
                        <div className={styles.actualInfoBlock}>
                            <div className={styles.infoHeading}>Объявления</div>
                            {announcements}
                        </div>
                    </div>
                    {(this.props.employeeSchedule.schedules.length !== 0) ? <div>
                        <Tabs defaultActiveKey="Lessons" >
                            <Tab eventKey="Lessons" title="Занятия" tabClassName={styles.customTabs} >
                                <div className={styles.scheduleTableHolder}>
                                    <div className={styles.gridHolder}>
                                        <ScheduleTable scheduleData={this.props.employeeSchedule.schedules} ifInGrid="true" filterWeeks={this.props.checkedWeeks} filterSubgroups={this.props.checkedSubgroups}/>                        
                                    </div>
                                    <div className={styles.filterHolder}>
                                        <FilterSchedule id={this.props.employeeSchedule.employee.id} fio={this.props.employeeSchedule.employee.fio} calendarId = {this.props.employeeSchedule.employee.calendarId}/>
                                    </div>
                                </div>
                            </Tab>
                            {/* ЭКЗАМЕНЫ!!!! */}
                            {/* {(this.props.employeeSchedule.examSchedules.length !== 0) ? <Tab eventKey="Session" title="Сессия" tabClassName={styles.customTabs} >
                                <div className={styles.scheduleTableHolder}>
                                    <div className={styles.gridHolder}>
                                        <ScheduleTable ifInGrid="true"/>                        
                                    </div>
                                    <div className={styles.filterHolder}>
                                        <FilterSchedule />
                                    </div>
                                </div>
                            </Tab> : null} */}
                        </Tabs>
                        <div className={styles.supportData}>
                            <div>тел. диспетчера: 
                                <a href="tel:+375172938933">{phoneNumberModification('+375172938933')}</a>, 
                                <a href="tel:+375172938044">{phoneNumberModification('+375172938044')}</a>, 
                                <a href="tel:+375172938034">{phoneNumberModification('+375172938034')}</a>
                            </div>
                            <div>Замечания и предложения по расписанию отправлять на <a href="mailto:disp@bsuir.by">disp@bsuir.by</a></div>
                        </div>
                    </div> : null}
                </div>
            }
            return (
                <div className={styles.employeeScheduleHolder}>
                    <h2 className={styles.heading}>РАСПИСАНИЕ ЗАНЯТИЙ ДЛЯ ПРЕПОДАВАТЕЛЯ</h2>
                    <h4 className={styles.yearHolder}>{getTerm()+' семестр, ' + getCurrentEducationalYear() + ' - ' + (getCurrentEducationalYear()+1)+' учебный год'}</h4>
                    <h4 className={styles.weekHolder}>Сейчас {this.props.weekNumber} учебная неделя</h4>
                    <AsyncTypeahead
                        id="async-employee-search"
                        filterBy={() => true}
                        className={styles.searchHolder}
                        isLoading={this.props.loadingEmployees}
                        emptyLabel={(!this.props.loadingEmployees) ? "Совпадения не найдены" : "Загрузка"}
                        labelKey={option => `${option.lastName} ${option.firstName} ${option.middleName}`}
                        minLength={1}
                        onSearch={(query) => {
                            this.props.onGetEmployees(query)}}
                        options={this.props.employees}
                        promptText="Загрузка"
                        searchText="Загрузка"
                        placeholder="Введите ФИО преподавателя..."
                        onChange={(s) => {
                            //localStorage.removeItem('selectedDepartment')
                            if (s.length !== 0) {
                                this.props.onGetEmployeeSchedule(s[0].id)
                                //localStorage.setItem('selectedDepartment', JSON.stringify(s[0]));
                            } else {
                                //clear LS
                            }
                            this.setState(state => {return {...state, selectedEmployee: s[0]}})
                        }   
                    }
                    renderMenuItemChildren={(option, props) => (
                        <React.Fragment>
                        <div>{option.lastName + ' '+ option.firstName + ' ' + option.middleName + ' (' + option.academicDepartment + ')'}</div>
                        </React.Fragment>
                    )}
                    />
                    {employeeSchedule}
                </div>
            )
        } else {
            return (
                <Alert className={styles.alertHolder} variant='danger'>
                                На данный момент расписание недоступно,<br/>
                                идёт формирование расписания на следующий семестр. <br/>
                                Расписание будет доступно после 15 августа.
                </Alert>
            )
        }
    }
};

const mapStateToProps = state => {
    return {
        employees: state.schedule.employees,
        loading: state.schedule.loading,
        loadingEmployees: state.schedule.loadingEmployees,
        weekNumber: state.schedule.weekNumber,
        employeeSchedule: state.schedule.employeeSchedule,
        employeeAnnouncements: state.schedule.employeeAnnouncements,
        checkedWeeks: state.schedule.checkedWeeks,
        checkedSubgroups: state.schedule.checkedSubgroups
    };
};

const mapDispatchToProps = dispatch => {
    return {
        onGetWeek: () => dispatch (actions.getWeekNumber()),
        onGetEmployees: (searchText) => dispatch(actions.getEmployees(searchText)),
        onGetEmployeeSchedule: (id) => dispatch(actions.getEmployeeSchedule(id)),
    };
};

export default connect(mapStateToProps,mapDispatchToProps)(employeeSchedule);