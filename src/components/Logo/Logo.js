import React from 'react';

import styles from './Logo.module.css';
import bsuir_logo from '../../assets/bsuir_logo.svg'


const logo = (props) => (
    <div onClick={props.clicked} className={styles.Logo}>
        <img width={props.size} src={bsuir_logo} alt="BSUIR Logo" />
    </div>
);

export default logo;