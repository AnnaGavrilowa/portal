import React from 'react';
import { Link } from 'react-router-dom';

import styles from './Footer.module.css';


const footer = ( props ) => (
    <footer className={styles.footer}>
        {/* <div className={styles.payLogoContainer}>
            <img className={styles.payLogoImg}
                src={pay_systems_logo}
                alt="Pay systems logo"/>
        </div> */}
        {/* <div className={styles.bsuirInfoFooter}>
            <div className={styles.bsuirLogoFooter}>
                <img src={bsuir_logo}
                    className={styles.bsuirLogoFooterImg}
                    alt="BSUIR Logo"/>
            <div className={styles.bsuirNameFooter}>
                УО "Белорусский государственный<br/>
                университет информатики<br/>
                и радиоэлектроники"<br/>
                УНН: 100383945
            </div>
            </div>
            <div className={styles.bsuirAddressFooter}>
            Республика Беларусь, Минск<br/>
            220013, ул. П. Бровки, 6
            </div>
        </div> */}
        {/* <div  className={styles.footerSpacer}> */}
            <div>
            <span><Link 
                to="/api"
                exact = "true" 
                className={styles.supportLink}>API</Link></span>
            <span><Link 
                to="/about"
                exact = "true" 
                className={styles.supportLink}>О нас</Link></span>
            </div>
            <div className={styles.supportInfo}>
                © 2009-2021 ОИТ ЦИИР БГУИР. Все права защищены.<br/>
                Телефон поддержки: <a className={styles.supportLink} href="tel:+375172932222"> +375 (17) 293-22-22</a><br/>
                <a className={styles.supportLink} href="https://www.bsuir.by/ru/informatsiya-o-rabote-s-sistemoy-edinoy-tekhnicheskoy-podderzhki"
                target="_blank" rel="noreferrer">Обратиться в поддержку</a>
            </div>
        {/* </div> */}
    </footer>
);
export default footer;