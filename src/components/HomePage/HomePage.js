import React from 'react';
import styles from './HomePage.module.css';

const homePage = ( props ) => {
        return (
            <div className={styles.informationPanel}>
                <div className={styles.headings}>Добро пожаловать в интегрированную информационную систему(ИИС)</div>
                <br/>
                <div className={styles.headings}>«БГУИР: Университет».</div>
                <h3 className={styles.info}>Более полную информацию об автоматизированных системах функционирующих в рамках ИИС «БГУИР: Университет» можно получить перейдя по следующей 
                    <br/><a className = {styles.tsiirPageLink} target="_blank" rel="noreferrer" href="https://www.bsuir.by/ru/tsiir">ссылке</a>
                </h3>
            </div>)
};

export default homePage;