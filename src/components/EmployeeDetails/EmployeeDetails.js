import React, {Component} from 'react';
import styles from './EmployeeDetails.module.css';
import { connect } from 'react-redux';
import Spinner from '../UI/Spinner/Spinner'
import DefaultIcon from '../../assets/default-photo.png'
import * as actions from '../../store/actions/index';
import Image from '../UI/ImageHolder/Image'
import {transform, phoneNumberModification} from '../../services/generalService'

class employeeDetails extends Component{
    componentDidMount() {
        let index = this.props.location.search.indexOf('=')
        let id
        if (index !== -1 ) id = this.props.location.search.substring(index+1)
        this.props.onGetEmployeeDetails(id)
    }

    createMarkup = (value) => {
        return {__html: value};
    }

    openProfile = () => {
        const filterQuery = `${this.props.employeeDetails.lastName}, ${this.props.employeeDetails.firstName.charAt(0)}. ${this.props.employeeDetails.middleName.charAt(0)}.`;
        window.open(`https://libeldoc.bsuir.by/simple-search?filterquery=${filterQuery}&filtername=author&filtertype=equals`);
    }

    render() {

        let information
        if (this.props.loading === true) information = <Spinner />
        if (this.props.employeeDetails && this.props.loading === false) {

            let jobList = []
            if (this.props.employeeDetails.jobPositions.length !== 0) {
                
                for (const [jobIndex, jobValue] of this.props.employeeDetails.jobPositions.entries()) {

                    let contactList = []
                    if(jobValue.contacts.lenth !== 0) {
                        for (const [contactIndex, contactValue] of jobValue.contacts.entries()) {
                            if (jobValue.contacts.length === 1) {
                                contactList.push(
                                    <div key={contactIndex}>
                                        <div>Рабочий телефон: <a href={'tel:' + contactValue.phoneNumber}>{phoneNumberModification(contactValue.phoneNumber)}</a></div>
                                        <div>{contactValue.address}</div>
                                    </div>)
                            } else {
                                contactList.push(
                                    <li key={contactIndex}>
                                        <div>Рабочий телефон: <a href={'tel:' + contactValue.phoneNumber}>{phoneNumberModification(contactValue.phoneNumber)}</a></div>
                                        {/* add yandex maps */}
                                        <div>{contactValue.address}</div>
                                    </li>)
                            }
                        }
                    }

                    jobList.push(
                        <div key={jobIndex}>
                            <div>Должность: {jobValue.jobPosition}</div>
                            <div>Место работы: {jobValue.department}</div>
                            {(jobValue.contacts.length > 1) ? <ul>{contactList}</ul> : <div>{contactList}</div>}
                        </div>
                    ) 
                }
            }

            let courses = []
            if (this.props.employeeDetails.readingCourses.length !== 0) {
                for (const [courseIndex, courseValue] of this.props.employeeDetails.readingCourses.entries()) {
                    courses.push(<li key={courseIndex}>{courseValue}</li>)
                }
            }

            let profiles = []
            if (this.props.employeeDetails.profileLinks.length !== 0) {
                for (const [profileIndex, profileValue] of this.props.employeeDetails.profileLinks.entries()) {
                    let faviconSrc = transform(profileValue.link)+'/favicon.ico'
                    profiles.push(<li key={profileIndex}>
                        <span><img className={styles.favicon} src={faviconSrc} alt="Profile link favicon"/></span>
                        <span><a href={profileValue.link}>{profileValue.profileLinkType}</a></span>
                    </li>)
                }
            }

            information = <div className={styles.container}>
                <h3>{this.props.employeeDetails.lastName + ' ' + this.props.employeeDetails.firstName + ' ' + this.props.employeeDetails.middleName}</h3>
                <div className={styles.imageHolder}><Image size={230} src={this.props.employeeDetails.photoLink} fallbackSrc={DefaultIcon} /></div>
                <div className={styles.personalDataHolder}>
                    {(this.props.employeeDetails.degree !== "") ? <div>{this.props.employeeDetails.degree + ', ' + this.props.employeeDetails.rank}</div> : null}
                    {(this.props.employeeDetails.email !== "") ? <div>Email: <a href={'mailto:'+this.props.employeeDetails.email}>{this.props.employeeDetails.email}</a></div> : null}
                    {jobList}
                    <h5>Ссылки на профили:</h5>
                    <ul>
                        <li>
                            <span><img className={styles.favicon} src="https://libeldoc.bsuir.by/favicon.ico" alt="Profile link favicon"/></span>
                            <span onClick={this.openProfile} className={styles.clickable}>Репозиторий БГУИР</span>
                        </li>
                        {profiles}
                    </ul>
                </div>

                {(this.props.employeeDetails.readingCourses.length !== 0) ? <div className={styles.content}>
                    <h5>Читаемые курсы</h5>
                    <ul>{courses}</ul>
                </div> : null}

                {(this.props.employeeDetails.researchArea) ? <div className={styles.content}>
                    <h5>Область профессиональных интересов/исследований</h5>
                    <div dangerouslySetInnerHTML={this.createMarkup(this.props.employeeDetails.researchArea)}></div>
                </div> : null}

                {(this.props.employeeDetails.education) ? <div className={styles.content}>
                    <h5>Образование</h5>
                    <div dangerouslySetInnerHTML={this.createMarkup(this.props.employeeDetails.education)}></div>
                </div> : null}

                {(this.props.employeeDetails.work) ? <div className={styles.content}>
                    <h5>Трудовая деятельность</h5>
                    <div dangerouslySetInnerHTML={this.createMarkup(this.props.employeeDetails.work)}></div>
                </div> : null}

                {(this.props.employeeDetails.achievements) ? <div className={styles.content}>
                    <h5>Заслуги, награды, поощрения</h5>
                    <div dangerouslySetInnerHTML={this.createMarkup(this.props.employeeDetails.achievements)}></div>
                </div> : null}

                {(this.props.employeeDetails.information) ? <div className={styles.content}>
                    <h5>Дополнительная информация</h5>
                    <div dangerouslySetInnerHTML={this.createMarkup(this.props.employeeDetails.information)}></div>
                </div> : null}

                {(this.props.employeeDetails.publications) ?  <div className={styles.content}>
                    <h5>Публикации</h5>
                    <div dangerouslySetInnerHTML={this.createMarkup(this.props.employeeDetails.publications)}></div>
                </div> : null}
            </div>
        }

        return (
            <React.Fragment>
                {information}
            </React.Fragment>
        )
    }
};

const mapStateToProps = state => {
    return {
        employeeDetails: state.employee.employeeDetails,
        loading: state.employee.loading
    };
};

const mapDispatchToProps = dispatch => {
    return {
        onGetEmployeeDetails: (id) => dispatch(actions.getEmployeeDetails(id)),
    };
};

export default connect(mapStateToProps,mapDispatchToProps)(employeeDetails);