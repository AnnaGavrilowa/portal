import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Table, Alert } from "react-bootstrap";
import { Typeahead } from 'react-bootstrap-typeahead';
import styles from './DepartmentAnnouncements.module.css';

import * as actions from '../../store/actions/index';

import Spinner from '../UI/Spinner/Spinner'

class departmentAnnouncements extends Component {

    state = {
        selectedDepartment: null,
    }

    componentDidMount() {
        this.props.onGetDepartments()
    }

    render() {

        let departmentAnnouncementsTable
        if (this.state.selectedDepartment === null) departmentAnnouncementsTable = null
        if (this.state.selectedDepartment !== null && this.props.loading === true) departmentAnnouncementsTable = <Spinner />
        if (this.state.selectedDepartment !== null && this.props.loading === false) {
            if(this.props.departmentAnnouncements.departmentAnnouncementsData.length !== 0) {

                let announcementListJSX = []
                for (const [index, value] of this.props.departmentAnnouncements.departmentAnnouncementsData.entries()) {
                    announcementListJSX.push(
                        <tr key = {index}>
                            <td>{value.date}</td>
                            <td>{value.content}</td>
                            <td>{value.employee}</td>
                        </tr>
                    )
                }

                departmentAnnouncementsTable = <Table bordered hover className={styles.tableWithAnnouncements}>
                    <thead>
                        <tr>
                            <th width="15%">Дата</th>
                            <th width="60%">Объявление</th>
                            <th width="25%">Преподаватель</th>
                        </tr>
                    </thead>
                    <tbody>
                        {announcementListJSX}
                    </tbody>
                </Table>
            } else {
                departmentAnnouncementsTable =  <Alert variant='danger'>
                                                    Объявления данной кафедры отсутствуют
                                                </Alert>
            }
        }
        
    return (
        <div className={styles.announcementsHolder}>
            <h2 className={styles.heading}>ОБЪЯВЛЕНИЯ КАФЕДРЫ</h2>
            <Typeahead
                className={styles.searchHolder}
                minLength={1}
                emptyLabel="Совпадения не найдены"
                id="basic-behaviors-example"
                labelKey="abbrev"
                options={this.props.departments}
                placeholder="Введите название кафедры"
                onChange={(s) => {
                        if (s.length !== 0)
                            this.props.onGetDepartmentAnnouncements(s[0].idDepartment)
                        this.setState(state => {return {...state, selectedDepartment: s}})
                    }
                }
            />
            {departmentAnnouncementsTable}
        </div>
    )}
};

const mapStateToProps = state => {
    return {
        departments: state.departments.departments,
        departmentAnnouncements: state.departments.departmentAnnouncements,
        loading: state.departments.loading
    };
};

const mapDispatchToProps = dispatch => {
    return {
        onGetDepartments: () => dispatch( actions.getDepartments() ),
        onGetDepartmentAnnouncements: (id) => dispatch (actions.getDepartmentAnnouncements(id))
    };
};

export default connect(mapStateToProps,mapDispatchToProps)(departmentAnnouncements);